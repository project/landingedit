(function($){
  Drupal.behaviors.landingEdit = {
    attach: function(context) {
      $('.landing-edit').once('landing-edit', function() {
        var $editable = $(this);
        $editable.parent('a').click(function() {return false;});
        var $content = $editable.find('.le-content');
        var $actions = $('<div>').addClass('le-actions');
        var $contentClone = '';
        if ($content.children().length == 0) {
          $content.wrapInner('<span></span>');
          $content.addClass('le-no-wrapper');
        }
        else {
          var float = $($content.children(0)).css('float');
          if (float != 'none') {
            $editable.css('float', float);
            $content.css('float', float);
          }
        }
        // Mark all original elements, so that we know they aren't generated with Javascript
        $('*', $content).each(function(i) {
          $(this).attr('data-le-original', i);
        });
        var $contentOriginal = $content.clone();
        // edit button
        var $edit = $('<a>')
          .attr('href', '#')
          .addClass('le-edit')
          .click(function(e) {
            e.preventDefault();
            $content.on('click', '*', {content: $content}, leEnableEdit);
            $editable.addClass('active');
            $contentClone = $content.html();
          });
        // save button
        var $save = $('<a>')
          .attr('id', $(this).data('landing-edit').replace(/__/g, '-'))
          .attr('href', '/' + $(this).data('landing-edit').replace(/__/g, '/'))
          .addClass('le-save ajax-processed')
          .click(function(e) {
            e.preventDefault();
            $editable.removeClass('active');
            // Remove content editable attributes if any and unbind edit activation handler
            $content.off('click', '*', leEnableEdit).removeAttr('contenteditable');
          });
        // Create a Drupal Ajax object for the save button
        var element_settings = {};
        if ($save.attr('href')) {
          element_settings.progress = { 'type': 'none' };
          element_settings.url = $save.attr('href');
          element_settings.event = 'click';
        }
        var base = $save.attr('id');
        Drupal.ajax[base] = new Drupal.ajax(base, $save, element_settings);
        // redefine beforeSerialize method to be able to grab the content
        Drupal.ajax[base].beforeSerialize = function(element, options) {
          Drupal.ajax.prototype.beforeSerialize(element, options);
          $('*', $content).each(function() {
            // Check if this element has been changed
            if ($(this).data('le-changed') == 1) {
              // clone the element
              var $element = $(this).clone();
              // Check if there are some tags inside this element
              // and replace them with original values,
              // allowing inner elements to be updated only if they were actually changed
              $element.children(':not(br)').each(function() {
                if ($(this).data('le-original') == undefined) {
                  $(this).remove();
                }
                else {
                  $(this).replaceWith($contentOriginal.find("[data-le-original='" + $(this).data('le-original') + "']"));
                }
              });
              $contentOriginal.find("[data-le-original='" + $(this).data('le-original') + "']").html($element.html());
            }
          });
          $('*', $contentOriginal).removeAttr('data-le-original').removeAttr('data-le-changed');
          if ($content.hasClass('le-no-wrapper')) {
            $contentOriginal.html($contentOriginal.children().html());
          }
          options.data['le_content'] = $contentOriginal.html();
        }
        // cancel button
        var $cancel = $('<a>')
          .attr('href', '#')
          .addClass('le-cancel')
          .click(function(e) {
            e.preventDefault();
            // Remove content editable attributes if any and unbind edit activation handler
            $content.off('click', '*', leEnableEdit).removeAttr('contenteditable');
            $editable.removeClass('active');
            $content.html($contentClone);
          });
        $actions.append($edit);
        $actions.append($save);
        $actions.append($cancel);
        $editable.prepend($actions);
      });
    }
  };

  function leEnableEdit(e) {
    e.preventDefault();
    e.stopPropagation();
    var $content = e.data.content;
    $('*', $content).removeAttr('contenteditable').unbind('keydown');
    $(this).on('keydown', function(e) {
      // trap the return key being pressed
      if (e.keyCode == 13) {
        document.execCommand('insertHTML', false, '<br/>');
        return false;
      }
    }).attr('contenteditable','true').focus();
    $(this).attr('data-le-changed', 1);
  }
})(jQuery)